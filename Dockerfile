FROM adoptopenjdk/openjdk11:latest
VOLUME /tmp

ARG DEPENDENCY_CLASS=target/dependecy
COPY ${DEPENDENCY_CLASS}/BOOT-INF/lib		/app/lib
COPY ${DEPENDENCY_CLASS}/META-INF			/app/META-INF
COPY ${DEPENDENCY_CLASS}/BOOT-INF			/app

CMD ["java","-Dspring.profiles.active=${SPRING_PROFILE}","-cp","app:app/lib/*", "com.bbpipeline.BBPipelineApplication"]