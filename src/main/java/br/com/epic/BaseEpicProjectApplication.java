package br.com.epic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseEpicProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseEpicProjectApplication.class, args);
	}

}
